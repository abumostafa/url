# URL Parser Library #

## Download & Installation ##
```
#!bash
git clone git@bitbucket.org:abumostafa/url.git
cd url
composer install

```

## Tests ##
```
#!bash
php vendor/bin/phpunit

```

## Demo ##
```
#!bash
php test.php

```