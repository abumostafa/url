<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL
 */
declare(strict_types = 1);

namespace URL;

class UrlConstants
{
    const URL_COMPONENTS = [
        self::COMPONENT_SCHEME   => null,
        self::COMPONENT_HOST     => null,
        self::COMPONENT_PATH     => null,
        self::COMPONENT_QUERY    => null,
        self::COMPONENT_PORT     => null,
        self::COMPONENT_FRAGMENT => null,
        self::COMPONENT_USER     => null,
        self::COMPONENT_PASS     => null,
    ];

    public const
        COMPONENT_SCHEME    = 'scheme',
        COMPONENT_HOST      = 'host',
        COMPONENT_PATH      = 'path',
        COMPONENT_QUERY     = 'query',
        COMPONENT_PORT      = 'port',
        COMPONENT_FRAGMENT  = 'fragment',
        COMPONENT_USER      = 'user',
        COMPONENT_PASS      = 'pass';
}