<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL
 */
declare(strict_types = 1);

namespace URL;

/**
 * URL decoder
 *
 * @package URL
 */
class Decoder
{
    /**
     * Decode given URL
     *
     * @param $url
     * @return string
     */
    public static function decode(string $url): string
    {
        return urldecode($url);
    }
}