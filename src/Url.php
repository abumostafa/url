<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL
 */
declare(strict_types = 1);

namespace URL;

use URL\Components;

/**
 * Parsed URL
 *
 * @package URL
 */
class Url
{
    protected
        /**
         * URL schema
         *
         * @var Components\Scheme
         */
        $schema,

        /**
         * URL host
         *
         * @var Components\Host
         */
        $host,

        /**
         * URL path
         *
         * @var Components\Path
         */
        $path,

        /**
         * URL query
         *
         * @var Components\Query
         */
        $query,

        /**
         * URL port
         *
         * @var Components\Port
         */
        $port,

        /**
         * URL fragment
         *
         * @var Components\Fragment
         */
        $fragment,

        /**
         * URL user info username
         *
         * @var Components\User
         */
        $user,

        /**
         * URL user info password
         *
         * @var Components\Pass
         */
        $pass;

    public function __construct(
        Components\Scheme $scheme,
        Components\Host $host,
        Components\Path $path,
        Components\Query $query,
        Components\Port $port,
        Components\Fragment $fragment,
        Components\User $user,
        Components\Pass $pass
    )
    {
        $this->schema = $scheme;
        $this->host = $host;
        $this->path = $path;
        $this->query = $query;
        $this->port = $port;
        $this->fragment = $fragment;
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * Create URL from URL string
     *
     * @param string $url
     * @return static
     */
    public static function createFromURL(string $url)
    {
        $components = self::parseURL($url);

        return new static(
            new Components\Scheme(
                $components[UrlConstants::COMPONENT_SCHEME]
            ),
            new Components\Host(
                $components[UrlConstants::COMPONENT_HOST]
            ),
            new Components\Path(
                $components[UrlConstants::COMPONENT_PATH]
            ),
            new Components\Query(
                $components[UrlConstants::COMPONENT_QUERY]
            ),
            new Components\Port(
                $components[UrlConstants::COMPONENT_PORT]
            ),
            new Components\Fragment(
                $components[UrlConstants::COMPONENT_FRAGMENT]
            ),
            new Components\User(
                $components[UrlConstants::COMPONENT_USER]
            ),
            new Components\Pass(
                $components[UrlConstants::COMPONENT_PASS]
            )
        );
    }

    /**
     * Parse given URL
     *
     * @param $url
     * @return array
     */
    public static function parseURL($url): array
    {
        return Parser::parse($url);
    }

    /**
     * Decode given URL
     *
     * @param string $url
     * @return string
     */
    public static function decodeURL(string $url): string
    {
        return Decoder::decode($url);
    }

    /**
     * @return Components\Scheme
     */
    public function getSchema(): ?Components\Scheme
    {
        return $this->schema;
    }

    /**
     * @param Components\Scheme $schema
     *
     * @return URL
     */
    public function setSchema(Components\Scheme $schema): URL
    {
        $this->schema = $schema;
        return $this;
    }

    /**
     * @return Components\Host
     */
    public function getHost(): ?Components\Host
    {
        return $this->host;
    }

    /**
     * @param Components\Host $host
     *
     * @return URL
     */
    public function setHost(Components\Host $host): URL
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return Components\Path
     */
    public function getPath(): ?Components\Path
    {
        return $this->path;
    }

    /**
     * @param Components\Path $path
     *
     * @return URL
     */
    public function setPath(Components\Path $path): URL
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return Components\Port
     */
    public function getPort(): ?Components\Port
    {
        return $this->port;
    }

    /**
     * @param Components\Port $port
     *
     * @return URL
     */
    public function setPort(Components\Port $port): URL
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return Components\User
     */
    public function getUser(): ?Components\User
    {
        return $this->user;
    }

    /**
     * @param Components\User $user
     *
     * @return URL
     */
    public function setUser(Components\User $user): URL
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Components\Pass
     */
    public function getPass(): ?Components\Pass
    {
        return $this->pass;
    }

    /**
     * @param Components\Pass $password
     *
     * @return URL
     */
    public function setPass(Components\Pass $password): URL
    {
        $this->pass = $password;
        return $this;
    }

    /**
     * @return Components\Query
     */
    public function getQuery(): Components\Query
    {
        return $this->query;
    }

    /**
     * @param Components\Query $query
     * @return Url
     */
    public function setQuery(Components\Query $query): URL
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @return Components\Fragment
     */
    public function getFragment(): Components\Fragment
    {
        return $this->fragment;
    }

    /**
     * @param Components\Fragment $fragment
     * @return Url
     */
    public function setFragment(Components\Fragment $fragment): URL
    {
        $this->fragment = $fragment;
        return $this;
    }

    /**
     * Convert URL to string
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s%s%s%s%s%s%s%s',
            $this->schema->getUriComponent(),
            $this->user->getUriComponent(),
            $this->pass->getUriComponent(),
            $this->host->getUriComponent(),
            $this->port->getUriComponent(),
            $this->path->getUriComponent(),
            $this->query->getUriComponent(),
            $this->fragment->getUriComponent()
        );
    }
}