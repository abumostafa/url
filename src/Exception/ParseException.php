<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Exception
 */
declare(strict_types = 1);

namespace URL\Exception;

use Exception;

/**
 * Parse Exception
 *
 * @package URL\Exception
 */
class ParseException extends Exception
{

}