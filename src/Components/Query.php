<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Components
 */
declare(strict_types = 1);

namespace URL\Components;

/**
 * URL query component
 *
 * @package URL\Components
 */
class Query extends AbstractComponent
{
    /**
     * {@inheritdoc}
     */
    public function getUriComponent(): string
    {
        $value = $this->__toString();

        return !empty($value) ? '?' . $value : '';
    }
}