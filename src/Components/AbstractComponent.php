<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Components
 */
declare(strict_types = 1);

namespace URL\Components;

use URL\Contracts\ComponentInterface;

/**
 * Abstract Component
 *
 * @package URL\Components
 */
class AbstractComponent implements ComponentInterface
{
    /**
     * @var string
     */
    protected $data;

    /**
     * AbstractComponent constructor.
     * @param string|null $data
     */
    public function __construct(?string $data = null)
    {
        $this->set($data);
    }

    /**
     * @param string $data
     */
    public function set($data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function get(): ?string
    {
        if (is_null($this->data) || !$this->data) {
            return null;
        }

        return (string) $this->data;
    }

    public function getUriComponent(): string
    {
        return $this->__toString();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return str_replace(null, '', $this->get());
    }
}