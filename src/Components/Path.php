<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Components
 */
declare(strict_types = 1);

namespace URL\Components;

/**
 * URL path component
 *
 * @package URL\Components
 */
class Path extends AbstractComponent
{
    public function getUriComponent(): string
    {
        return $this->__toString();
    }
}