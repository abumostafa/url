<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Contracts
 */
declare(strict_types = 1);

namespace URL\Contracts;

interface ComponentArrayInterface extends ComponentInterface
{
    /**
     * Return the component as an array
     *
     * @return array
     */
    public function toArray();

    /**
     * Return all the keys or a subset of the keys of an array
     *
     * @return array
     */
    public function keys();
}