<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Contracts
 */
declare(strict_types = 1);

namespace URL\Contracts;

interface QueryInterface extends ComponentArrayInterface
{

}