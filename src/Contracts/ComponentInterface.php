<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Contracts
 */
declare(strict_types = 1);

namespace URL\Contracts;

/**
 * Interface ComponentInterface
 * @package URL\Contracts
 */
interface ComponentInterface
{
    /**
     * set component value
     *
     * @param string $value
     */
    public function set($value): void;

    /**
     * return component value
     *
     * @return string
     */
    public function get(): ?string;

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * @return null|string
     */
    public function getUriComponent(): string;
}