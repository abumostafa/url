<?php
/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL
 */
declare(strict_types = 1);

namespace URL;

use URL\Exception\ParseException;

/**
 * URL Parser
 *
 * @package URL
 */
class Parser
{
    /**
     * Parse given URL
     *
     * @param string $url
     * @return array
     *
     * @throws ParseException
     */
    public static function parse(string $url): array
    {
        $cleanURL = self::sanitizeURL($url);

        if (empty($cleanURL) || !$urlParts = self::extractURL($cleanURL)) {
            throw new ParseException(sprintf('Failed to parse URL "%s"', $url));
        }

        return array_merge(UrlConstants::URL_COMPONENTS, $urlParts);
    }

    /**
     * Extract URL string to URL parts
     *
     * @param string $url
     * @return array
     *
     */
    protected static function extractURL(string $url): array
    {
        if (!$urlParts = parse_url($url)) {
            // fallback parser
        }

        return $urlParts;
    }

    /**
     * Prepare URL before parse
     *
     * @param string $url
     * @return string
     */
    protected static function sanitizeURL(string $url): string
    {
        $cleanURL = URL::decodeURL($url);
        return trim($cleanURL);
    }
}