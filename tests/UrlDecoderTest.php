<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests
 */
namespace URL\Tests;

use URL\Decoder;

class UrlDecoderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test URL decoder
     */
    public function testDecode()
    {
        $urls = [
            'https%3A%2F%2Fwww.example.com%2Fbirth'             => 'https://www.example.com/birth',
            'https%3A%2F%2Fwww.example.com%2F'                  => 'https://www.example.com/',
            'http%3A%2F%2Fexample.com%2Fbattle%2Fbomb.php'      => 'http://example.com/battle/bomb.php',
            'http%3A%2F%2Fwww.example.edu%2F'                   => 'http://www.example.edu/',
            'https%3A%2F%2Fwww.example.com%2Fargument.php'      => 'https://www.example.com/argument.php',
            'https%3A%2F%2Fexample.com%2Fadvice'                => 'https://example.com/advice',
            'https%3A%2F%2Famount.example.com%2F'               => 'https://amount.example.com/',
            'https%3A%2F%2Fexample.org%2F%3Fbubble%3Dbear'      => 'https://example.org/?bubble=bear',
            'https%3A%2F%2Fbattle.example.com%2Fanger'          => 'https://battle.example.com/anger',
            'http%3A%2F%2Fwww.example.com%2Farch%2Fafternoon'   => 'http://www.example.com/arch/afternoon',
            '%2F%2Ffoo%3Abar%40w1.superman.com%3A9090%2Fvery%2Flong%2Fpath.html%3Fp1%3Dv1%26p2%3Dv2%23more-details' => '//foo:bar@w1.superman.com:9090/very/long/path.html?p1=v1&p2=v2#more-details',
        ];

        foreach($urls as $actual => $expected) {

            $this->assertEquals($expected, Decoder::decode($actual));
        }
    }
}