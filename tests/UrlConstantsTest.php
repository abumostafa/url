<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests
 */
namespace URL\Tests;

use URL\UrlConstants;

/**
 * Url Constants Test
 *
 * @package URL\Tests
 */
class UrlConstantsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test url components constant
     */
    public function testUrlComponents()
    {
        $this->assertEquals([
            'scheme' => null,
            'user' => null,
            'pass' => null,
            'host' => null,
            'port' => null,
            'path' => null,
            'query' => null,
            'fragment' => null,
        ], UrlConstants::URL_COMPONENTS);
    }

    /**
     * Test parsed url components names
     */
    public function testComponentsNames()
    {
        $this->assertEquals(UrlConstants::COMPONENT_SCHEME, 'scheme');
        $this->assertEquals(UrlConstants::COMPONENT_USER, 'user');
        $this->assertEquals(UrlConstants::COMPONENT_PASS, 'pass');
        $this->assertEquals(UrlConstants::COMPONENT_HOST, 'host');
        $this->assertEquals(UrlConstants::COMPONENT_PORT, 'port');
        $this->assertEquals(UrlConstants::COMPONENT_PATH, 'path');
        $this->assertEquals(UrlConstants::COMPONENT_QUERY, 'query');
        $this->assertEquals(UrlConstants::COMPONENT_FRAGMENT, 'fragment');
    }
}