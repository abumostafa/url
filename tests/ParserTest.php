<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests
 */
namespace URL\Tests;

use URL\Exception\ParseException;
use URL\Parser;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    public function testParseEmpty()
    {
        $this->expectException(ParseException::class);
        Parser::parse('');
    }

    public function testParseArrayKeys()
    {
        $urls = [
            'http://google.com',
            'https://www.google.com',
            'data:image/gif;base64,R0lGOD lhCwAOAMQfAP////7+/vj4+Hh4eHd3d/v7+/Dw8HV1dfLy8ubm5vX19e3t7fr 6+nl5edra2nZ2dnx8fMHBwYODg/b29np6eujo6JGRkeHh4eTk5LCwsN3d3dfX 13Jycp2dnevr6////yH5BAEAAB8ALAAAAAALAA4AAAVq4NFw1DNAX/o9imAsB tKpxKRd1+YEWUoIiUoiEWEAApIDMLGoRCyWiKThenkwDgeGMiggDLEXQkDoTh CKNLpQDgjeAsY7MHgECgx8YR8oHwNHfwADBACGh4EDA4iGAYAEBAcQIg0Dk gcEIQA7'
        ];

        foreach ($urls as $url) {

            $parsedURL = Parser::parse($url);

            $this->assertArrayHasKey('scheme', $parsedURL);
            $this->assertArrayHasKey('user', $parsedURL);
            $this->assertArrayHasKey('pass', $parsedURL);
            $this->assertArrayHasKey('host', $parsedURL);
            $this->assertArrayHasKey('port', $parsedURL);
            $this->assertArrayHasKey('path', $parsedURL);
            $this->assertArrayHasKey('query', $parsedURL);
            $this->assertArrayHasKey('fragment', $parsedURL);
        }
    }

    public function testParsedScheme()
    {
        $urls = [
            'http://www.olx.com/' => [
                'scheme' => 'http',
                'user' => '',
                'pass' => '',
                'host' => 'www.olx.com',
                'port' => '',
                'path' => '/',
                'query' => '',
                'fragment' => '',
            ],
            'https://www.olx.com/' => [
                'scheme' => 'https',
                'user' => '',
                'pass' => '',
                'host' => 'www.olx.com',
                'port' => '',
                'path' => '/',
                'query' => '',
                'fragment' => '',
            ],
            'ftp://192.168.50.50' => [
                'scheme' => 'ftp',
                'user' => '',
                'pass' => '',
                'host' => '192.168.50.50',
                'port' => '',
                'path' => '',
                'query' => '',
                'fragment' => '',
            ],
            'burger:cheeseburger' => [
                'scheme' => 'burger',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => 'cheeseburger',
                'query' => '',
                'fragment' => '',
            ],
            'mailto:webmaster@example.com' => [
                'scheme' => 'mailto',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => 'webmaster@example.com',
                'query' => '',
                'fragment' => '',
            ],
            'soldat://127.0.0.1:23073/thatssecret' => [
                'scheme' => 'soldat',
                'user' => '',
                'pass' => '',
                'host' => '127.0.0.1',
                'port' => '23073',
                'path' => '/thatssecret',
                'query' => '',
                'fragment' => '',
            ],
            'fxqn:/us/va/reston/cnri/ietf/24/asdf%*.fred' => [
                'scheme' => 'fxqn',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => '/us/va/reston/cnri/ietf/24/asdf%*.fred',
                'query' => '',
                'fragment' => '',
            ],
            'news:12345667123%asdghfh@info.cern.ch' => [
                'scheme' => 'news',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => '12345667123%asdghfh@info.cern.ch',
                'query' => '',
                'fragment' => '',
            ],
        ];

        foreach ($urls as $url => $parts) {

            $parsedURL = Parser::parse($url);

            $this->assertEquals($parts['scheme'], $parsedURL['scheme']);
            $this->assertEquals($parts['user'], $parsedURL['user']);
            $this->assertEquals($parts['pass'], $parsedURL['pass']);
            $this->assertEquals($parts['host'], $parsedURL['host']);
            $this->assertEquals($parts['port'], $parsedURL['port']);
            $this->assertEquals($parts['path'], $parsedURL['path']);
            $this->assertEquals($parts['query'], $parsedURL['query']);
            $this->assertEquals($parts['fragment'], $parsedURL['fragment']);
        }
    }
}