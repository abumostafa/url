<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests\Components
 */
namespace URL\Tests\Components;

use URL\Components\Pass;
use URL\Contracts\ComponentInterface;

class PassTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ComponentInterface
     */
    protected $component;

    public function setUp()
    {
        $this->component = $this->createComponent();
    }

    public function testGetDefaultNull()
    {
        $this->assertNull($this->component->get());
    }

    public function testSetterSame()
    {
        $data = [
            'secret' => 'secret',
            'password  ddd' => 'password  ddd',
            '23^&@#&KJHjk123' => '23^&@#&KJHjk123',
            '   sdsd    ' => '   sdsd    ',
            ',q12./dfasdasd' => ',q12./dfasdasd',
            '        ' => '        ',
            '' => null,
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->get());
        }
    }

    public function testSetterNotSame()
    {
        $data = [
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertNotSame($expected, $this->component->get());
        }
    }

    public function testSetterConstructorSame()
    {
        $data = [
            'secret' => 'secret',
            'password  ddd' => 'password  ddd',
            '23^&@#&KJHjk123' => '23^&@#&KJHjk123',
            '   sdsd    ' => '   sdsd    ',
            ',q12./dfasdasd' => ',q12./dfasdasd',
            '        ' => '        ',
            '' => null,
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertSame($expected, $component->get());
        }
    }

    public function testSetterConstructorNotSame()
    {
        $data = [
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertNotSame($expected, $component->get());
        }
    }

    public function testComponentUriSame()
    {
        $data = [
            'secret' => ':secret@',
            'password  ddd' => ':password  ddd@',
            '23^&@#&KJHjk123' => ':23^&@#&KJHjk123@',
            '   sdsd    ' => ':   sdsd    @',
            ',q12./dfasdasd' => ':,q12./dfasdasd@',
            ':@' => '::@@',
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->getUriComponent());
        }
    }

    public function testComponentUriNotSame()
    {
        $examples = [
            '' => ':@',
        ];

        foreach ($examples as $protocol => $uriComponent) {

            $this->component->set($protocol);
            $this->assertNotSame($uriComponent, $this->component->getUriComponent());
        }
    }

    protected function createComponent($value = null)
    {
        return new Pass($value);
    }
}