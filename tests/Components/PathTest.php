<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests\Components
 */
namespace URL\Tests\Components;

use URL\Components\Path;
use URL\Contracts\ComponentInterface;

class PathTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ComponentInterface
     */
    protected $component;

    public function setUp()
    {
        $this->component = $this->createComponent();
    }

    public function testGetDefault()
    {
        $this->assertEmpty($this->component->get());
    }

    public function testSetterSame()
    {
        $data = [
            '/path/to/dir'  => '/path/to/dir',
            '/one'          => '/one',
            '/'             => '/',
            ''              => null,
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->get());
        }
    }

    public function testSetterNotSame()
    {
        $data = [
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertNotSame($expected, $this->component->get());
        }
    }

    public function testSetterConstructorSame()
    {
        $data = [
            '/path/to/dir'  => '/path/to/dir',
            '/one'          => '/one',
            '/'             => '/',
            ''              => null,
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertSame($expected, $component->get());
        }
    }

    public function testSetterConstructorNotSame()
    {
        $data = [
            '' => '/',
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertNotSame($expected, $component->get());
        }
    }

    public function testComponentUriSame()
    {
        $data = [
            '/path/to/dir'  => '/path/to/dir',
            '/one'          => '/one',
            '/'             => '/',
            ''              => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->getUriComponent());
        }
    }

    public function testComponentUriNotSame()
    {
        $examples = [
            '' => null,
        ];

        foreach ($examples as $protocol => $uriComponent) {

            $this->component->set($protocol);
            $this->assertNotSame($uriComponent, $this->component->getUriComponent());
        }
    }

    protected function createComponent($value = null)
    {
        return new Path($value);
    }

}