<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests\Components
 */
namespace URL\Tests\Components;

use URL\Components\Scheme;
use URL\Contracts\ComponentInterface;

class SchemeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ComponentInterface
     */
    protected $component;

    public function setUp()
    {
        $this->component = $this->createComponent();
    }

    public function testGetDefaultNull()
    {
        $this->assertNull($this->component->get());
    }

    public function testSetterSame()
    {
        $data = [
            'http' => 'http',
            'https' => 'https',
            'some-scheme' => 'some-scheme',
            'ftp' => 'ftp',
            'ftps' => 'ftps',
            '' => null,
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->get());
        }
    }

    public function testSetterNotSame()
    {
        $data = [
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertNotSame($expected, $this->component->get());
        }
    }

    public function testSetterConstructorSame()
    {
        $data = [
            'ms432'         => 'ms432',
            'facetime'      => 'facetime',
            'dns'           => 'dns',
            'file'          => 'file',
            'gizmoproject'  => 'gizmoproject',
            'h323'          => 'h323',
            ''              => null,
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertSame($expected, $component->get());
        }
    }

    public function testSetterConstructorNotSame()
    {
        $data = [
            '    '          => '',
            'dns'           => 'DNS',
            ''              => false,
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertNotSame($expected, $component->get());
        }
    }

    public function testComponentUriSame()
    {
        $data = [
            'tel' => 'tel://',
            'ddd' => 'ddd://',
            'chrome-extension' => 'chrome-extension://',
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->getUriComponent());
        }
    }

    public function testComponentUriNotSame()
    {
        $examples = [
            'mailto' => 'mailto//',
            'mob' => 'mob:',
            'ms' => 'ms:///',
            'chrome' => 'chrome',
            '' => '://',
        ];

        foreach ($examples as $protocol => $uriComponent) {

            $this->component->set($protocol);
            $this->assertNotSame($uriComponent, $this->component->getUriComponent());
        }
    }

    protected function createComponent($value = null)
    {
        return new Scheme($value);
    }
}