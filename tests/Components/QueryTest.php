<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests\Components
 */
namespace URL\Tests\Components;

use URL\Components\Query;
use URL\Contracts\ComponentInterface;

class QueryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ComponentInterface
     */
    protected $component;

    public function setUp()
    {
        $this->component = $this->createComponent();
    }

    public function testGetDefaultNull()
    {
        $this->assertEmpty($this->component->get());
    }

    public function testSetterSame()
    {
        $data = [
            'foo=bar'                 => 'foo=bar',
            'foo[]=bar&foo[]=bar&bar=foo' => 'foo[]=bar&foo[]=bar&bar=foo',
            'blah=holy,das&foo=noo'   => 'blah=holy,das&foo=noo',
            'file=file.php&dir=/path/to/bash/dir' => 'file=file.php&dir=/path/to/bash/dir',
            ''                        => null,
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->get());
        }
    }

    public function testSetterNotSame()
    {
        $data = [
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertNotSame($expected, $this->component->get());
        }
    }

    public function testSetterConstructorSame()
    {
        $data = [
            'foo=bar'                 => 'foo=bar',
            'foo[]=bar&foo[]=bar&bar=foo' => 'foo[]=bar&foo[]=bar&bar=foo',
            'blah=holy,das&foo=noo'   => 'blah=holy,das&foo=noo',
            'file=file.php&dir=/path/to/bash/dir' => 'file=file.php&dir=/path/to/bash/dir',
            ''                        => null,
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertSame($expected, $component->get());
        }
    }

    public function testSetterConstructorNotSame()
    {
        $data = [
            '' => '',
        ];

        foreach ($data as $actual => $expected) {

            $component = $this->createComponent($actual);
            $this->assertNotSame($expected, $component->get());
        }
    }

    public function testComponentUriSame()
    {
        $data = [
            'foo=bar'                 => '?foo=bar',
            'foo=bar&foo=bar&bar=foo' => '?foo=bar&foo=bar&bar=foo',
            'blah=holy,das&foo=noo'   => '?blah=holy,das&foo=noo',
            'file=file.php&dir=/path/to/bash/dir' => '?file=file.php&dir=/path/to/bash/dir',
            ''                        => '',
        ];

        foreach ($data as $actual => $expected) {

            $this->component->set($actual);
            $this->assertSame($expected, $this->component->getUriComponent());
        }
    }

    public function testComponentUriNotSame()
    {
        $examples = [
            '' => '?',
        ];

        foreach ($examples as $protocol => $uriComponent) {

            $this->component->set($protocol);
            $this->assertNotSame($uriComponent, $this->component->getUriComponent());
        }
    }

    protected function createComponent($value = null)
    {
        return new Query($value);
    }
}