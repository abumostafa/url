<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           14/01/2017
 * @project        URL
 * @package        URL\Tests
 */
namespace URL\Tests;

use URL\Components\Fragment;
use URL\Components\Host;
use URL\Components\Pass;
use URL\Components\Path;
use URL\Components\Port;
use URL\Components\Query;
use URL\Components\Scheme;
use URL\Components\User;
use URL\Exception\ParseException;
use URL\Url;

class UrlTest extends \PHPUnit_Framework_TestCase
{
    public function testParseEmpty()
    {
        $this->expectException(ParseException::class);
        Url::parseURL('');
    }

    public function testParsedInstances()
    {
        $urls = [
            'http://google.com',
            'https://www.google.com',
            'data:image/gif;base64,R0lGOD lhCwAOAMQfAP////7+/vj4+Hh4eHd3d/v7+/Dw8HV1dfLy8ubm5vX19e3t7fr 6+nl5edra2nZ2dnx8fMHBwYODg/b29np6eujo6JGRkeHh4eTk5LCwsN3d3dfX 13Jycp2dnevr6////yH5BAEAAB8ALAAAAAALAA4AAAVq4NFw1DNAX/o9imAsB tKpxKRd1+YEWUoIiUoiEWEAApIDMLGoRCyWiKThenkwDgeGMiggDLEXQkDoTh CKNLpQDgjeAsY7MHgECgx8YR8oHwNHfwADBACGh4EDA4iGAYAEBAcQIg0Dk gcEIQA7'
        ];

        foreach ($urls as $url) {

            $parsedURL = Url::createFromURL($url);

            $this->assertInstanceOf(Url::class, $parsedURL);

            $this->assertInstanceOf(Scheme::class, $parsedURL->getSchema());
            $this->assertInstanceOf(User::class, $parsedURL->getUser());
            $this->assertInstanceOf(Pass::class, $parsedURL->getPass());
            $this->assertInstanceOf(Host::class, $parsedURL->getHost());
            $this->assertInstanceOf(Port::class, $parsedURL->getPort());
            $this->assertInstanceOf(Path::class, $parsedURL->getPath());
            $this->assertInstanceOf(Query::class, $parsedURL->getQuery());
            $this->assertInstanceOf(Fragment::class, $parsedURL->getFragment());
        }
    }

    public function testCreateFromUrl()
    {
        $urls = [
            'http://www.olx.com/' => [
                'scheme' => 'http',
                'user' => '',
                'pass' => '',
                'host' => 'www.olx.com',
                'port' => '',
                'path' => '/',
                'query' => '',
                'fragment' => '',
            ],
            'https://www.olx.com/' => [
                'scheme' => 'https',
                'user' => '',
                'pass' => '',
                'host' => 'www.olx.com',
                'port' => '',
                'path' => '/',
                'query' => '',
                'fragment' => '',
            ],
            'ftp://192.168.50.50' => [
                'scheme' => 'ftp',
                'user' => '',
                'pass' => '',
                'host' => '192.168.50.50',
                'port' => '',
                'path' => '',
                'query' => '',
                'fragment' => '',
            ],
            'burger:cheeseburger' => [
                'scheme' => 'burger',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => 'cheeseburger',
                'query' => '',
                'fragment' => '',
            ],
            'mailto:webmaster@example.com' => [
                'scheme' => 'mailto',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => 'webmaster@example.com',
                'query' => '',
                'fragment' => '',
            ],
            'soldat://127.0.0.1:23073/thatssecret' => [
                'scheme' => 'soldat',
                'user' => '',
                'pass' => '',
                'host' => '127.0.0.1',
                'port' => '23073',
                'path' => '/thatssecret',
                'query' => '',
                'fragment' => '',
            ],
            'fxqn:/us/va/reston/cnri/ietf/24/asdf%*.fred' => [
                'scheme' => 'fxqn',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => '/us/va/reston/cnri/ietf/24/asdf%*.fred',
                'query' => '',
                'fragment' => '',
            ],
            'news:12345667123%asdghfh@info.cern.ch' => [
                'scheme' => 'news',
                'user' => '',
                'pass' => '',
                'host' => '',
                'port' => '',
                'path' => '12345667123%asdghfh@info.cern.ch',
                'query' => '',
                'fragment' => '',
            ],
        ];

        foreach ($urls as $url => $parts) {

            $parsedURL = Url::createFromURL($url);

            $this->assertEquals($parts['scheme'], $parsedURL->getSchema()->get());
            $this->assertEquals($parts['user'], $parsedURL->getUser()->get());
            $this->assertEquals($parts['pass'], $parsedURL->getPass()->get());
            $this->assertEquals($parts['host'], $parsedURL->getHost()->get());
            $this->assertEquals($parts['port'], $parsedURL->getPort()->get());
            $this->assertEquals($parts['path'], $parsedURL->getPath()->get());
            $this->assertEquals($parts['query'], $parsedURL->getQuery()->get());
            $this->assertEquals($parts['fragment'], $parsedURL->getFragment()->get());
        }
    }
}