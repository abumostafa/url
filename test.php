<?php

require 'vendor/autoload.php';

use URL\Url;

$urls = [
    'http://www.google.com',
    'http://foo:bar@w1.superman.com:8080/very/long/path.html?p1=v1&p2=v2#more-details',
    'https://secured.com:443',
    'ftp://ftp.bogus.com/~some/path/to/a/file.txt',
    'chrome-extension://<extensionID>/<pageName>.html',
    'example.com',
    '%2F%2Ffoo%3Abar%40w1.superman.com%3A9090%2Fvery%2Flong%2Fpath.html%3Fp1%3Dv1%26p2%3Dv2%23more-details',
    '//example.org:81/hi?a=b#c=d',
    '//example.org/hi?a=b#c=d',
];

foreach ($urls as $url) {
    $parsedURL = Url::createFromURL($url);

    echo sprintf("Original URL: %s%s", $url, PHP_EOL);
    echo sprintf("Restored URL: %s%s", $parsedURL, PHP_EOL);
    echo sprintf("Scheme: %s%s", $parsedURL->getSchema(), PHP_EOL);
    echo sprintf("User: %s%s", $parsedURL->getUser(), PHP_EOL);
    echo sprintf("Pass: %s%s", $parsedURL->getPass(), PHP_EOL);
    echo sprintf("Host: %s%s", $parsedURL->getHost(), PHP_EOL);
    echo sprintf("Port: %s%s", $parsedURL->getPort(), PHP_EOL);
    echo sprintf("Path: %s%s", $parsedURL->getPath(), PHP_EOL);
    echo sprintf("Query: %s%s", $parsedURL->getQuery(), PHP_EOL);
    echo sprintf("Fragment: %s%s", $parsedURL->getFragment(), PHP_EOL);
    echo PHP_EOL;
}
